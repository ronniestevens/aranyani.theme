
# needed for the CSSColorClass
from Products.Five.browser import BrowserView
from zope.component import getUtility
from plone.registry.interfaces import IRegistry

from plone import api
from plone.multilingual.interfaces import ILanguage


class HomepageView(BrowserView):

    def homepagepage(self):
        context = self.context
        catalog = api.portal.get_tool('portal_catalog')
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        res = catalog(
            portal_type='Document',
            Subject='homepage',
            path=dict(query=path, depth=10))
        try:
            page = res[0]
            language = ILanguage(context).get_language()
            obj = page.getObject()
        except IndexError:
            return None
        try:
            return dict(title=page.Title,
                        body=obj.text,
                        language=language,
                        description=page.Description)
        except AttributeError:
            return None

    def homepageevents(self):
        context = self.context
        catalog = api.portal.get_tool('portal_catalog')
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        result = []
        for event in catalog(
            {'portal_type': 'Event',
             'sort_on': 'start',
             'sort_limit': 5,
             'sort_order': 'ascending',
             'path': dict(query=path, depth=10)}
        ):
            result.append(
                dict(title=event.Title,
                     url=event.getURL(),
                     description=event.Description,
                     date=event.start)
            )
        return result

    def homepagepublications(self):
        context = self.context
        catalog = api.portal.get_tool('portal_catalog')
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        result = []
        for pub in catalog(
            {'portal_type': 'publication',
             'sort_on': 'getObjPositionInParent',
             'sort_limit': 5,
             'sort_order': 'ascending',
             'path': dict(query=path, depth=10)}
        ):
            obj = pub.getObject()
            result.append(
                dict(title=pub.Title,
                     url=pub.getURL(),
                     year=obj.year)
            )
        return result


class NewsView(BrowserView):

    def newslist(self):
        context = self.context
        catalog = api.portal.get_tool('portal_catalog')
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        result = []
        for news in catalog(
            {'portal_type': 'News Item',
             'sort_on': 'Date',
             'sort_order': 'ascending',
             'path': dict(query=path, depth=10)}):
            obj = news.getObject()
            result.append(
                dict(title=news.Title,
                     url=news.getURL(),
                     description=news.Description,
                     date=news.start,
                     image=obj.image,
                     image_url="%s/@@images/image" % news.getURL())
            )
        return result

    def news_rows(self):
        return self.groupby(self.newslist(), groupsize=2)

    def groupby(self, iterable, groupsize=2):
        result = []
        subgroup = []
        for item in iterable:
            subgroup.append(item)
            if len(subgroup) % groupsize == 0:
                result.append(tuple(subgroup))
                subgroup = []
        if len(subgroup) > 0:
            result.append(tuple(subgroup))
        return result


class ColorView(BrowserView):

    def getcolor(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            color = registry[
                'aranyani.theme.interfaces.IaranyaniThemeSettings.themeColor'
            ]

        return color
