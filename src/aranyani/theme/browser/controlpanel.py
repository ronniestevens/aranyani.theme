from plone.app.registry.browser.controlpanel import RegistryEditForm
from plone.app.registry.browser.controlpanel import ControlPanelFormWrapper

from aranyani.theme.interfaces import IaranyaniThemeSettings
from plone.z3cform import layout
from z3c.form import form

class aranyaniThemeControlPanelForm(RegistryEditForm):
    form.extends(RegistryEditForm)
    schema = IaranyaniThemeSettings

aranyaniThemeControlPanelView = layout.wrap_form(aranyaniThemeControlPanelForm, ControlPanelFormWrapper)
aranyaniThemeControlPanelView.label = u"aranyani theme settings"