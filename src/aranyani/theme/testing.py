from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class aranyanithemeLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import aranyani.theme
        xmlconfig.file(
            'configure.zcml',
            aranyani.theme,
            context=configurationContext
        )

        # Install products that use an old-style initialize() function
        #z2.installProduct(app, 'Products.PloneFormGen')

#    def tearDownZope(self, app):
#        # Uninstall products installed above
#        z2.uninstallProduct(app, 'Products.PloneFormGen')

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'aranyani.theme:default')

aranyani_THEME_FIXTURE = aranyanithemeLayer()
aranyani_THEME_INTEGRATION_TESTING = IntegrationTesting(
    bases=(aranyani_THEME_FIXTURE,),
    name="aranyanithemeLayer:Integration"
)
